
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
CURR_DIR="${PWD}"
PROJECT_DIR=${SCRIPT_DIR}/..
image_file=${PROJECT_DIR}/image_files/2019-07-01_20-56-37_raspbian_backup.img
#image_file=${PROJECT_DIR}/image_files/2019-07-10-raspbian-buster-full.img
TMP_FOLDER=$( mktemp -d )
TMP_FILE0=$TMP_FOLDER/IMAGE0.IMG
TMP_FILE1=$TMP_FOLDER/IMAGE1.IMG

function mount_partitions {
    printf "Creating temp folders...\n"
    mkdir -p $PROJECT_DIR/work/part0
    mkdir -p $PROJECT_DIR/work/part1

    printf "Calculating partitions offsets (in sectors)...\n"
    START_SECTOR=( $( fdisk -l $image_file | grep "$image_file" | awk 'FNR>1 {print $2}' ) )

    echo "OFFSET0': ${START_SECTOR[0]}"
    echo "OFFSET1': ${START_SECTOR[1]}"

    printf "Calculating partitions offsets (in bytes)...\n"
    OFFSET0=$((${START_SECTOR[0]} * 512))
    OFFSET1=$((${START_SECTOR[1]} * 512))

    echo "OFFSET0: ${OFFSET0}"
    echo "OFFSET1: ${OFFSET1}"

    printf "Mounting partitions...\n"
    printf " => sudo mount -v -o offset=${OFFSET0} -t vfat $image_file ${PROJECT_DIR}/work/part0\n"
    printf " => sudo mount -v -o offset=${OFFSET1} -t ext4 $image_file ${PROJECT_DIR}/work/part1\n"
    sudo mount -v -o offset=${OFFSET0} -t vfat $image_file ${PROJECT_DIR}/work/part0
    #sudo mount -v -o offset=${OFFSET1} -t ext4 $image_file ${PROJECT_DIR}/work/part1
}

function umount_partitions {
    printf "Umounting partitions...\n"
    #sudo umount ${PROJECT_DIR}/work/part0
    #sudo umount ${PROJECT_DIR}/work/part1
}

mount_partitions

ls -la ${PROJECT_DIR}/work
ls -la ${PROJECT_DIR}/work/part0
ls -la ${PROJECT_DIR}/work/part1

umount_partitions

cd $CURR_DIR
