#!/bin/bash

## Preparation
#
# sudo apt-get install f3
#

# sudo dd if=/dev/zero of=/dev/mmcblk0 bs=512 count=1 conv=notrunc status=progress

#sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | sudo fdisk /dev/mmcblk0
#  o # clear the in memory partition table
#  n # new partition
#  p # primary partition
#  1 # partition number 1
#    # default - start at beginning of disk 
#    # all size
#  w # write the partition table
#  q # and we're done
#EOF
#
#retVal=$?
#
#if [ $retVal -ne 0 ]; then
#    echo "Error cleaning sd card data"
#    exit $retVal
#fi

SD_PARTITION=$( sudo fdisk -l | grep -Eo '^/dev/mm[a-zA-Z0-9]*' )

sudo mkfs -t vfat $SD_PARTITION

retVal=$?

if [ $retVal -ne 0 ]; then
    echo "Error creating vfat partition"
    exit $retVal
fi

mkdir /tmp/disk

sudo mount $SD_PARTITION /tmp/disk

sudo f3write /tmp/disk
sudo f3read /tmp/disk

sudo umount $SD_PARTITION

rm -rf /tmp/disk
