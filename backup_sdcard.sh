#!/bin/bash

function error {
    error=$1
    notify-send --urgency=critical "hey you!" "Holy crap!! You screwed something...\n${error}"
    exit 1
}

filename=$1

[ -z "$filename" ] && filename="raspbian"



today=`date +%Y-%m-%d_%H-%M-%S`
device="/dev/mmcblk0"

backup_img_file="./image_files/${today}_${filename}_backup.img"

echo $backup_img_file

mkdir image_files

echo "Copying device to image file"
sudo dd if=${device} of=${backup_img_file} bs=1M status=progress || error "Error copying data to image file"

CURR_USER="${USER}"

sudo chown ${CURR_USER}:${CURR_} ${backup_img_file}

echo "Packing image as a .tar.bz2"
tar -jcvf ${backup_img_file}.tar.bz2 ${backup_img_file}

echo "Calculating device tar.bz2 checksum"
sudo sha1sum ${backup_img_file}.tar.bz2 > ${backup_img_file}.tar.bz2.sha1

echo "Process finished."

notify-send --urgency=low "hey you!" "I finished to make these... Things. Take a look!"
