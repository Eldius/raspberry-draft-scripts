# Draft notes and scripts for Raspberry Pi #

## useful links ##

https://linuxreviews.org/HOWTO_test_SD_cards_and_identify_fake_ones_(mostly_sold_on_ebay)#cite_note-1

https://linuxhint.com/mount_partition_uuid_label_linux/
https://tecadmin.net/format-usb-in-linux/
https://www.reddit.com/r/archlinux/comments/1lafu2/fstab_how_to_automount_but_ignore_if_its_not/

https://github.com/transmission-remote-gui/transgui#command-line-parameters

https://eiosifidis.blogspot.com/2014/02/seedbox-raspberry-pi-lighttpd-rtorrent-rutorrent.html

- [Installing K3s Kubernetes cluster on Raspberry Pi 4 - rpi4cluster.com](https://rpi4cluster.com/k3s/k3s-kube-setting/)
- [How to gracefully remove a node from Kubernetes? - StackOverflow](https://stackoverflow.com/questions/35757620/how-to-gracefully-remove-a-node-from-kubernetes/54220808#54220808)
- [How to build a Raspberry Pi Kubernetes Cluster with k3s - Medium](https://medium.com/thinkport/how-to-build-a-raspberry-pi-kubernetes-cluster-with-k3s-76224788576c)

- [Production like Kubernetes on Raspberry Pi: Load-balancer - Medium](https://michael-tissen.medium.com/production-like-kubernetes-on-raspberry-pi-load-balancer-ae3ba8883a52)
- [Raspberry Pi Kubernetes Cluster with K3s and Metallb - Medium](https://medium.com/@kevinlutzer9/raspberry-pi-kubernetes-cluster-with-k3s-and-metallb-1dab9ef475bb)
- []


## snippets ##

```
## master
# curl -sfL https://get.k3s.io | sh -s - --write-kubeconfig-mode 644 --disable servicelb --token b41059c8-ab28-11ee-b571-23bbe0a6e39e --node-taint CriticalAddonsOnly=true:NoExecute --bind-address 192.168.100.196 --disable-cloud-controller --disable local-storage

curl -sfL https://get.k3s.io | sh -s - --write-kubeconfig-mode 644 --token b41059c8-ab28-11ee-b571-23bbe0a6e39e --bind-address 192.168.100.196


## node1
curl -sfL https://get.k3s.io | K3S_URL=https://192.168.100.196:6443 K3S_TOKEN=b41059c8-ab28-11ee-b571-23bbe0a6e39e sh -
```

```
b41059c8-ab28-11ee-b571-23bbe0a6e39e
```
