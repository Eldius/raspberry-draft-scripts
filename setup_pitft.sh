#!/bin/bash

CURR_DIR=\${PWD}

cd /tmp
wget https://raw.githubusercontent.com/adafruit/Raspberry-Pi-Installer-Scripts/master/adafruit-pitft.sh
chmod +x adafruit-pitft.sh
./adafruit-pitft.sh
