#!/bin/bash

today=`date +%Y-%m-%d_%H-%M-%S`

mkdir image_files

echo "Copying device to image file"
sudo dd if=/dev/mmcblk0 of=./image_files/${today}_backupd.img bs=1M status=progress

sudo chown ${USER}:${USER} ${today}_backupd.img

echo "Packing image as a .tar.bz2"
tar -jcvf ./image_files/${today}_backupd.img.tar.bz2 ./image_files/${today}_backupd.img

echo "Process finished."
