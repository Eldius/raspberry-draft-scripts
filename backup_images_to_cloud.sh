#/bin/bash

rclone copy \
    -v \
    --filter "+ *.bz2" \
    --filter "+ *.sha1" \
    --filter "- *.img" \
    --filter "- *.zip" \
    --filter "- *.gz" \
    --filter "- *.ini" \
        ./image_files/ \
        eldius:/dev/raspberry
