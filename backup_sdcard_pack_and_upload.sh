#!/bin/bash

function error {
    error=$1
    notify-send --urgency=critical "hey you!" "Holy crap!! You screwed something...\n${error}"
    exit 1
}

function process {
    filename=$1

    [ -z "$filename" ] && error "invalid filename"

    echo "Packing image as a .tar.bz2"
    tar -jcvf ${filename}.tar.bz2 ${filename} || error "failed to pack file ${filename}"

    echo "Calculating device tar.bz2 checksum"
    sudo sha1sum ${filename}.tar.bz2 > ${filename}.tar.bz2.sha1 || error "failed to calc checksum for file ${filename}"

    rclone copy \
        -v \
        --filter "+ *.bz2" \
        --filter "+ *.sha1" \
        --filter "- *.img" \
        --filter "- *.zip" \
        --filter "- *.gz" \
        --filter "- *.ini" \
            ./image_files/ \
            eldius:/dev/raspberry || error "failed to upload file ${filename}"

    rm $filename

    echo "Process finished for file ${filename}."
}

for filename in ./image_files/*.img; do
    echo "processing ${filename}"
    process $filename
done

notify-send --urgency=low "hey you!" "I finished to make these... Things. Take a look!"
