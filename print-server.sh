#!/bin/bash

#git clone https://github.com/goodtft/LCD-show.git && cd LCD-show
#chmod +x ./LCD-hdmi
#./LCD-hdmi

sudo apt-get install -y hplip python-qtpy python3-qtpy
# /usr/share/hplip/base/password.py
# 'debian': 'su', => 'debian': 'sudo',

sudo hp-plugin -i

sudo apt-get install -y cups
sudo usermod -a -G lpadmin pi

sudo cupsctl --remote-any
sudo /etc/init.d/cups restart

sudo apt-get install -y samba

sudo mv /etc/samba/smb.conf /etc/samba/smb.conf.bak

sudo cat <<EOF >> /etc/samba/smb.conf
# CUPS printing.
[printers]
comment = All Printers
browseable = no
path = /var/spool/samba
printable = yes
guest ok = yes
read only = yes
create mask = 0700

# Windows clients look for this share name as a source of downloadable
# printer drivers
[print$]
comment = Printer Drivers
path = /var/lib/samba/printers
browseable = yes
read only = no
guest ok = no
EOF

sudo /etc/init.d/samba restart

