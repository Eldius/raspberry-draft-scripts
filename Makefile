
testsd:
	$(eval MOUNT_POINT = $(shell lsblk | grep 'mmcblk0p' | awk '{ print $$7 }'))
	@echo "MOUNT_POINT: $(MOUNT_POINT)"
	[ -z "$(MOUNT_POINT)" ] && exit 1
	f3write $(MOUNT_POINT)
	f3read $(MOUNT_POINT)
