#!/bin/bash

ssh pi@192.168.1.18 sudo systemctl stop python-daemon

sftp pi@192.168.1.18 <<ENDSFTP
    cd /app/python/daemon/
    rm *.py
    rm execution.log
    put *.py
ENDSFTP

#ssh pi@192.168.1.18 sudo systemctl start python-daemon
#sleep 5
#ssh pi@192.168.1.18 sudo journalctl -u python-daemon
#ssh pi@192.168.1.18 sudo cat /app/python/daemon/execution.log

#ssh pi@192.168.1.18 /app/python/daemon/main.py

ssh pi@192.168.1.18 sudo reboot
