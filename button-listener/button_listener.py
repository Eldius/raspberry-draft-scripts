#!/usr/bin/env python

from time import sleep
from RPi import GPIO
from os import sys

GPIO.setmode(GPIO.BCM)
GPIO.setup( 1, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup( 2, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup( 3, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup( 4, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup( 5, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup( 6, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup( 7, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup( 8, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup( 9, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(10, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(11, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(12, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(13, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(14, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(15, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(16, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(19, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(20, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(21, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(25, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(26, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(27, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

original = sys.stdout
sys.stdout = open('/var/log/gpio-tests.log', 'w')

def my_callback(channel):
    sleep(1.5)  # confirm the movement by waiting 1.5 sec 
    if GPIO.input(4): # and check again the input
        print("pressed button:  4")

        # stop detection for 20 sec
        #GPIO.remove_event_detect(4)
        sleep(20)
        #GPIO.add_event_detect(4, GPIO.RISING, callback=my_callback, bouncetime=300)

    if GPIO.input(5): # and check again the input
        print("pressed button:  5")

        # stop detection for 20 sec
        #GPIO.remove_event_detect(5)
        sleep(20)
        #GPIO.add_event_detect(5, GPIO.RISING, callback=my_callback, bouncetime=300)

    if GPIO.input(17): # and check again the input
        print("pressed button: 17")

        # stop detection for 20 sec
        #GPIO.remove_event_detect(17)
        sleep(20)
        #GPIO.add_event_detect(17, GPIO.RISING, callback=my_callback, bouncetime=300)

    if GPIO.input(22): # and check again the input
        print("pressed button: 22")

        # stop detection for 20 sec
        #GPIO.remove_event_detect(22)
        sleep(20)
        #GPIO.add_event_detect(22, GPIO.RISING, callback=my_callback, bouncetime=300)

    if GPIO.input(23): # and check again the input
        print("pressed button: 23")

        # stop detection for 20 sec
        #GPIO.remove_event_detect(23)
        sleep(20)
        #GPIO.add_event_detect(23, GPIO.RISING, callback=my_callback, bouncetime=300)

    if GPIO.input(24): # and check again the input
        print("pressed button: 24")

        # stop detection for 20 sec
        #GPIO.remove_event_detect(24)
        sleep(20)
        #GPIO.add_event_detect(24, GPIO.RISING, callback=my_callback, bouncetime=300)

GPIO.add_event_detect( 4, GPIO.RISING, callback=my_callback, bouncetime=300)
GPIO.add_event_detect( 5, GPIO.RISING, callback=my_callback, bouncetime=300)
GPIO.add_event_detect(17, GPIO.RISING, callback=my_callback, bouncetime=300)
GPIO.add_event_detect(22, GPIO.RISING, callback=my_callback, bouncetime=300)
GPIO.add_event_detect(23, GPIO.RISING, callback=my_callback, bouncetime=300)
GPIO.add_event_detect(24, GPIO.RISING, callback=my_callback, bouncetime=300)

# you can continue doing other stuff here
while True:
    pass

#  4: \/
#  5: X
# 17:
# 22: []
# 23: o
# 24: /\
# 27: OUT
