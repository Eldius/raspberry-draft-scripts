#!/usr/bin/env python
import os
from os import sys
import getpass
import time

logf = open(os.path.dirname(os.path.realpath(__file__)) + "/exception.log", "w")

def setup_output():
    output_tty = "/dev/tty1"
    log_file = os.path.dirname(os.path.realpath(__file__)) + "/execution.log"
    sys.stdout = open(log_file, 'w')
    with open(output_tty, 'rb') as inf, open(output_tty, 'wb') as outf:
        os.dup2(inf.fileno(), 0)
        os.dup2(outf.fileno(), 1)
        os.dup2(outf.fileno(), 2)

def main(stdscr):
    global ex
    try:
        curses.curs_set(0)
    except Exception, e:
        ex = e

    field = curses.newwin(1, 20, 1, 1)
    field.addstr(0, 0, "Hello, world!", curses.A_REVERSE) 
    field.refresh()
    field.getch()

if __name__ == '__main__':     # Program start from here

    try:
        setup_output()

        import curses
        import listener
        #curses.wrapper(listener.main)
        listener.setup()
        print("starting again...")
        print("current user: " + getpass.getuser())
        print("entering loop...")
        while True:
            time.sleep(10)
            pass
        listener.destroy()
    except Exception as e:
        logf.write(e)
        print(e)
        listener.destroy()
