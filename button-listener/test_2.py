import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(4, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(5, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(27,GPIO.OUT) 
 
# set up the colors
BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
RED   = (255,   0,   0)
GREEN = (  0, 255,   0)
BLUE  = (  0,   0, 255)
 
while True:
    if (not GPIO.input(5)):
        print("5" + "X")
    if (not GPIO.input(22)):
        print("22 " + u"\u25A1")
    if (not GPIO.input(23)):
        print("23 " + "O")
    if (not GPIO.input(24)):
        print("24 " + u"\u1403")
    if (not GPIO.input(4)):
        print("4 " + u"\u140D")
        GPIO.output(27,GPIO.HIGH)
    if (not GPIO.input(17)):
        print("17 " + u"\u140E")
        GPIO.output(27,GPIO.LOW)

    time.sleep(0.1)
