from time import sleep
from RPi import GPIO

GPIO.setmode(GPIO.BCM) # Use physical pin numbering
GPIO.setup( 4, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin  4 to be an input pin and set initial value to be pulled low (off)
GPIO.setup( 5, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin  5 to be an input pin and set initial value to be pulled low (off)
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin 17 to be an input pin and set initial value to be pulled low (off)
GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin 22 to be an input pin and set initial value to be pulled low (off)
GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin 23 to be an input pin and set initial value to be pulled low (off)
GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin 24 to be an input pin and set initial value to be pulled low (off)



def main_menu():
    GPIO.add_event_detect( 4,GPIO.FALLING,callback=shutdown_menu) # Setup event on pin  4 rising edge
    GPIO.add_event_detect( 5,GPIO.FALLING,callback=lambda x: button_callback( 5)) # Setup event on pin  5 rising edge
    GPIO.add_event_detect(17,GPIO.FALLING,callback=lambda x: button_callback(17)) # Setup event on pin 17 rising edge
    GPIO.add_event_detect(22,GPIO.FALLING,callback=lambda x: button_callback(22)) # Setup event on pin 22 rising edge
    GPIO.add_event_detect(23,GPIO.FALLING,callback=lambda x: button_callback(23)) # Setup event on pin 23 rising edge
    GPIO.add_event_detect(24,GPIO.FALLING,callback=lambda x: button_callback(24)) # Setup event on pin 24 rising edge

def shutdown_menu(channel):
    print("Deseja realmente desligar?")
    print("<= Sim")
    print("<= Nao")
    GPIO.setmode(GPIO.BCM) # Use physical pin numbering
    GPIO.setup( 5, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin  5 to be an input pin and set initial value to be pulled low (off)
    GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin  4 to be an input pin and set initial value to be pulled low (off)
    GPIO.add_event_detect( 5,GPIO.FALLING,callback=lambda x: main_menu()) # Setup event on pin  5 rising edge
    GPIO.add_event_detect( 24,GPIO.FALLING,callback=lambda x: shutdown(24)) # Setup event on pin  5 rising edge

def shutdown(channel):
    os.system('systemctl poweroff') 

def button_callback(channel):
    print(0, "Button was pushed!")
    print(1, 'Scored! %s'%channel)
    sleep(10)
    main_menu()


def clear():
    GPIO.remove_event_detect( 4) # Setup event on pin  4 rising edge
    GPIO.remove_event_detect( 5) # Setup event on pin  5 rising edge
    GPIO.remove_event_detect(17) # Setup event on pin 17 rising edge
    GPIO.remove_event_detect(22) # Setup event on pin 22 rising edge
    GPIO.remove_event_detect(23) # Setup event on pin 23 rising edge
    GPIO.remove_event_detect(24) # Setup event on pin 24 rising edge

if __name__ == '__main__':
    main_menu()
