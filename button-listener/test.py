#!/usr/bin/env python

from time import sleep
from RPi import GPIO
from os import sys

def setup():
    print("SETTING UP THE GPIO PINS...")
    GPIO.setwarnings(False) # Ignore warning for now
    #GPIO.setmode(GPIO.BOARD) # Use physical pin numbering
    GPIO.setmode(GPIO.BCM) # Use physical pin numbering
    GPIO.setup( 4, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin  4 to be an input pin and set initial value to be pulled low (off)
    GPIO.setup( 5, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin  5 to be an input pin and set initial value to be pulled low (off)
    GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin 17 to be an input pin and set initial value to be pulled low (off)
    GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin 22 to be an input pin and set initial value to be pulled low (off)
    GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin 23 to be an input pin and set initial value to be pulled low (off)
    GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin 24 to be an input pin and set initial value to be pulled low (off)

    GPIO.add_event_detect( 4,GPIO.FALLING,callback=lambda x: button_callback( 4)) # Setup event on pin  4 rising edge
    GPIO.add_event_detect( 5,GPIO.FALLING,callback=lambda x: button_callback( 5)) # Setup event on pin  5 rising edge
    GPIO.add_event_detect(17,GPIO.FALLING,callback=lambda x: button_callback(17)) # Setup event on pin 17 rising edge
    GPIO.add_event_detect(22,GPIO.FALLING,callback=lambda x: button_callback(22)) # Setup event on pin 22 rising edge
    GPIO.add_event_detect(23,GPIO.FALLING,callback=lambda x: button_callback(23)) # Setup event on pin 23 rising edge
    GPIO.add_event_detect(24,GPIO.FALLING,callback=lambda x: button_callback(24)) # Setup event on pin 24 rising edge
    GPIO.setmode(GPIO.BCM) # Use physical pin numbering

# you can continue doing other stuff here
#  4: \/
#  5: X
# 17:
# 22: []
# 23: o
# 24: /\
# 27: OUT

def button_callback(channel):
    print("Button was pushed!")
    print('Scored! %s'%channel)

def destroy():
    exit

if __name__ == '__main__':     # Program start from here
    setup()
    try:
        message = raw_input("Press enter to quit\n\n") # Run until someone presses enter
        print(message)
        GPIO.cleanup() # Clean up
    except KeyboardInterrupt:  # When 'Ctrl+C' is pressed, the child program destroy() will be  executed.
        destroy()
