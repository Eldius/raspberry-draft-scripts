#!/usr/bin/env python
from time import sleep
from RPi import GPIO
import curses
import os
import urllib2

stdscr = curses.initscr()
curses.noecho()
curses.cbreak()
stdscr.keypad(True)
stdscr.clear()
stdscr.refresh()


curses.start_color()
curses.init_pair(1, curses.COLOR_CYAN, curses.COLOR_BLACK)
curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_WHITE)
curses.init_pair(4, curses.COLOR_GREEN, curses.COLOR_BLACK)
curses.init_pair(5, curses.COLOR_BLACK, curses.COLOR_GREEN)

begin_x = 2
begin_y = 2
height , width = stdscr.getmaxyx()
win = curses.newwin(height - 2, width - 2, begin_y, begin_x)

win.border()

def setup():
    main_menu()

GPIO.setmode(GPIO.BCM) # Use physical pin numbering
GPIO.setup( 4, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin  4 to be an input pin and set initial value to be pulled low (off)
GPIO.setup( 5, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin  5 to be an input pin and set initial value to be pulled low (off)
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin 17 to be an input pin and set initial value to be pulled low (off)
GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin 22 to be an input pin and set initial value to be pulled low (off)
GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin 23 to be an input pin and set initial value to be pulled low (off)
GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin 24 to be an input pin and set initial value to be pulled low (off)


# you can continue doing other stuff here
#  4: \/
#  5: X
# 17:
# 22: []
# 23: o
# 24: /\
# 27: OUT
#def write(msg):

def main_menu():
    clear()
    GPIO.add_event_detect( 4,GPIO.FALLING,callback=shutdown_menu) # Setup event on pin  4 rising edge
    GPIO.add_event_detect( 5,GPIO.FALLING,callback=lambda x: button_callback( 5)) # Setup event on pin  5 rising edge
    GPIO.add_event_detect(17,GPIO.FALLING,callback=lambda x: button_callback(17)) # Setup event on pin 17 rising edge
    GPIO.add_event_detect(22,GPIO.FALLING,callback=lambda x: button_callback(22)) # Setup event on pin 22 rising edge
    GPIO.add_event_detect(23,GPIO.FALLING,callback=lambda x: internet_on(23)) # Setup event on pin 23 rising edge
    GPIO.add_event_detect(24,GPIO.FALLING,callback=lambda x: button_callback(24)) # Setup event on pin 24 rising edge

    stdscr.addstr( 0, 0, "<= Test internet connection", curses.color_pair(4))
    stdscr.addstr( 4, 0, "<= Opcao 02", curses.color_pair(4))
    stdscr.addstr( 8, 0, "<= Opcao 03", curses.color_pair(4))
    stdscr.addstr(11, 0, "<= Opcao 04", curses.color_pair(4))
    stdscr.refresh()


def write(line, msg):
    stdscr.addstr(line, 0, msg, curses.color_pair(4))
    stdscr.refresh()
    print(msg)

def writeStr(msg):
    stdscr.addstr(msg + "\n", curses.color_pair(4))
    stdscr.refresh()
    print(msg)

def clear():
    stdscr.clear()
    GPIO.remove_event_detect( 4) # Setup event on pin  4 rising edge
    GPIO.remove_event_detect( 5) # Setup event on pin  5 rising edge
    GPIO.remove_event_detect(17) # Setup event on pin 17 rising edge
    GPIO.remove_event_detect(22) # Setup event on pin 22 rising edge
    GPIO.remove_event_detect(23) # Setup event on pin 23 rising edge
    GPIO.remove_event_detect(24) # Setup event on pin 24 rising edge

def internet_on(channel):
    clear()
    try:
        urllib2.urlopen('https://google.com', timeout=1)
        stdscr.addstr( 0, 0, "[Google] Internet connection is ok", curses.color_pair(4))
        stdscr.refresh()
        urllib2.urlopen('https://facebook.com', timeout=1)
        stdscr.addstr( 0, 0, "[Facebook] Internet connection is ok", curses.color_pair(4))
        stdscr.refresh()
        sleep(10)
    except urllib2.URLError as err: 
        return False
    finally:
        main_menu()

def shutdown_menu(channel):
    clear()
    stdscr.clear()
    stdscr.addstr( 0, 0, "Deseja realmente desligar?", curses.color_pair(4))
    stdscr.addstr( 8, 0, "<= Sim", curses.color_pair(4))
    stdscr.addstr(11, 0, "<= Nao", curses.color_pair(4))
    GPIO.setmode(GPIO.BCM) # Use physical pin numbering
    GPIO.setup( 5, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin  5 to be an input pin and set initial value to be pulled low (off)
    GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Set pin  4 to be an input pin and set initial value to be pulled low (off)
    GPIO.add_event_detect( 5,GPIO.FALLING,callback=lambda x: main_menu()) # Setup event on pin  5 rising edge
    GPIO.add_event_detect( 24,GPIO.FALLING,callback=lambda x: shutdown(24)) # Setup event on pin  5 rising edge
    stdscr.refresh()

def shutdown(channel):
    clear()
    os.system('systemctl poweroff') 

def button_callback(channel):
    clear()
    write(0, "Button was pushed!")
    write(1, 'Scored! %s'%channel)
    sleep(10)
    main_menu()

def destroy():
    clear()
    GPIO.cleanup() # Clean up
    curses.endwin()
    exit

