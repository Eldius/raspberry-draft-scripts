#!/bin/bash

temp_folder=$( mktemp -d )

img_mount_point=${temp_folder}/image

mkdir -p ${img_mount_point}

unzip image_files/2019-06-20-raspbian-buster.zip -d ${temp_folder}

mount -t ext3 -o loop ${temp_folder}/2019-06-20-raspbian-buster.img ${img_mount_point}
